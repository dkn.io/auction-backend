package controllers

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	models "api_beego/models"
	"time"
)

type AuctionsController struct {
	beego.Controller
}

type ambilAuctions struct {
    Auction_id int
	Coll_id int
	From_date time.Time
	Due_date time.Time
	Auction_method int
	Down_payment int
	Description string
	Auction_count int
	Status int 
	Accepted_bidder int
	Acc_id  int
	Type_id int	
	Owner_name string
	Coll_location string
	Initial_col_price int 
	Final_col_price int
	Ljk_id int
	Document_path string
	Username string
	Email string
	Type_name string
	Auction_method_name string
	Accepted_bidder_name string
}

func (api *AuctionsController) GetAllAuctions() {
    o := orm.NewOrm()
	o.Using("default")
	var Auctions [] ambilAuctions
	var sql string
	sql = "select auctions.*,collateral.*,option_method.name as auction_method_name,useraccount.username as accepted_bidder_name from auctions"
	sql += " left join m_option as option_method on option_method.no  = auctions.auction_method and option_method.type = 2"
	sql += " left join collateral on auctions.coll_id  = collateral.coll_id"
	sql += " left join useraccount on useraccount.user_id  = auctions.accepted_bidder"
	num, err := o.Raw(sql).QueryRows(&Auctions)
	if err != orm.ErrNoRows && num > 0 {
		api.Data["json"] = Auctions	
	}
	
	api.ServeJSON()
}

func (api *AuctionsController) GetAuctionsByID() {
    chk := api.GetString("auction_id")
    if chk == "" {
        api.Ctx.WriteString("")
        return
    }
    o := orm.NewOrm()
	o.Using("default")
	var Auctions [] ambilAuctions
	var sql string
	sql = "select auctions.*,collateral.*,option_method.name as auction_method_name,useraccount.username as accepted_bidder_name from auctions"
	sql += " left join m_option as option_method on option_method.no  = auctions.auction_method and option_method.type = 2"
	sql += " left join collateral on auctions.coll_id  = collateral.coll_id"
	sql += " left join useraccount on useraccount.user_id  = auctions.accepted_bidder where auctions.auction_id = '"+api.GetStrings("auction_id")[0]+"'"
	num, err := o.Raw(sql).QueryRows(&Auctions)
	if err != orm.ErrNoRows && num > 0 {
		api.Data["json"] = Auctions[0]		
	}
	api.ServeJSON()
}

func (api *AuctionsController) GetAuctionsByCollateral() {
    chk := api.GetString("coll_id")
    if chk == "" {
        api.Ctx.WriteString("")
        return
    }
    o := orm.NewOrm()
	o.Using("default")
	var Auctions [] ambilAuctions
	var sql string
	sql = "select auctions.*,collateral.*,option_method.name as auction_method_name,useraccount.username as accepted_bidder_name from auctions"
	sql += " left join m_option as option_method on option_method.no  = auctions.auction_method and option_method.type = 2"
	sql += " left join collateral on auctions.coll_id  = collateral.coll_id"
	sql += " left join useraccount on useraccount.user_id  = auctions.accepted_bidder where auctions.coll_id = '"+api.GetStrings("coll_id")[0]+"'"
	num, err := o.Raw(sql).QueryRows(&Auctions)
	if err != orm.ErrNoRows && num > 0 {
		api.Data["json"] = Auctions	
	}
	api.ServeJSON()
}

func (api *AuctionsController) GetAuctionsByDueDate() {
    chk := api.GetString("due_date")
    if chk == "" {
        api.Ctx.WriteString("")
        return
    }
    o := orm.NewOrm()
	o.Using("default")
	var Auctions [] ambilAuctions
	var sql string
	sql = "select auctions.*,collateral.*,option_method.name as auction_method_name,useraccount.username as accepted_bidder_name from auctions"
	sql += " left join m_option as option_method on option_method.no  = auctions.auction_method and option_method.type = 2"
	sql += " left join collateral on auctions.coll_id  = collateral.coll_id"
	sql += " left join useraccount on useraccount.user_id  = auctions.accepted_bidder where auctions.due_date = '"+api.GetStrings("due_date")[0]+"'"
	num, err := o.Raw(sql).QueryRows(&Auctions)
	if err != orm.ErrNoRows && num > 0 {
		api.Data["json"] = Auctions
	}
	api.ServeJSON()
}

func (api *AuctionsController) CreateAuctions() {
    coll_id := api.GetString("coll_id")
    if coll_id == "" {
        api.Ctx.WriteString("")
        return
    }
	from_date := api.GetString("from_date")
    if from_date == "" {
        api.Ctx.WriteString("")
        return
    }
	due_date := api.GetString("due_date")
    if due_date == "" {
        api.Ctx.WriteString("")
        return
    }
	auction_method := api.GetString("auction_method")
    if auction_method == "" {
        api.Ctx.WriteString("")
        return
    }
	down_payment := api.GetString("down_payment")
    if down_payment == "" {
        api.Ctx.WriteString("")
        return
    }
	description := api.GetString("description")
    if description == "" {
        api.Ctx.WriteString("")
        return
    }
	status := api.GetString("status")
    if status == "" {
        api.Ctx.WriteString("")
        return
    }
    o := orm.NewOrm()
	o.Using("default")
	var Auctions []*models.Auctions
	var sql string
	sql = "INSERT INTO auctions (coll_id, from_date, due_date, auction_method, down_payment, description, status) VALUES ('"+api.GetStrings("coll_id")[0]+"'"
	sql += ",'"+api.GetStrings("from_date")[0]+"','"+api.GetStrings("due_date")[0]+"','"+api.GetStrings("auction_method")[0]+"','"+api.GetStrings("down_payment")[0]+"'"
	sql += ",'"+api.GetStrings("description")[0]+"','"+api.GetStrings("status")[0]+"')"
	o.Raw(sql).QueryRows(&Auctions)
	api.Data["json"] = 1
	api.ServeJSON()
}

func (api *AuctionsController) EditAuctions() {
    coll_id := api.GetString("coll_id")
    if coll_id == "" {
        api.Ctx.WriteString("")
        return
    }
	from_date := api.GetString("from_date")
    if from_date == "" {
        api.Ctx.WriteString("")
        return
    }
	due_date := api.GetString("due_date")
    if due_date == "" {
        api.Ctx.WriteString("")
        return
    }
	auction_method := api.GetString("auction_method")
    if auction_method == "" {
        api.Ctx.WriteString("")
        return
    }
	down_payment := api.GetString("down_payment")
    if down_payment == "" {
        api.Ctx.WriteString("")
        return
    }
	status := api.GetString("status")
    if status == "" {
        api.Ctx.WriteString("")
        return
    }
	accepted_bidder := api.GetString("accepted_bidder")
    if accepted_bidder == "" {
        api.Ctx.WriteString("")
        return
    }
	description := api.GetString("description")
    if description == "" {
        api.Ctx.WriteString("")
        return
    }
	auction_id := api.GetString("auction_id")
    if auction_id == "" {
        api.Ctx.WriteString("")
        return
    }
	auction_count := api.GetString("auction_count")
    if auction_count == "" {
        api.Ctx.WriteString("")
        return
    }
    o := orm.NewOrm()
	o.Using("default")
	var Auctions []*models.Auctions
	var sql string
	sql = "select auctions_id from auctions where auctions_id = '"+api.GetString("auction_id")+"'";
	num, err := o.Raw(sql).QueryRows(&Auctions)
	if err != orm.ErrNoRows && num > 0 {
		api.Data["json"] = ""
	    api.ServeJSON()   
	}
	sql = "UPDATE auctions SET coll_id = '"+api.GetStrings("coll_id")[0]+"', from_date = '"+api.GetStrings("from_date")[0]+"', due_date = '"+api.GetStrings("due_date")[0]+"'"
	sql += ", auction_method = '"+api.GetStrings("auction_method")[0]+"', down_payment = '"+api.GetStrings("down_payment")[0]+"', description = '"+api.GetStrings("description")[0]+"'"
	sql += ", auction_count = '"+api.GetStrings("auction_count")[0]+"', status = '"+api.GetStrings("status")[0]+"', accepted_bidder = '"+api.GetStrings("accepted_bidder")[0]+"'" 
	sql += " where auction_id = '"+api.GetStrings("auction_id")[0]+"'"
	o.Raw(sql).QueryRows(&Auctions)
	api.Data["json"] = "successfully edit auctions with auction_id = "+api.GetStrings("auction_id")[0]
	api.ServeJSON()
}

func (api *AuctionsController) DeleteAuction() {
    chk := api.GetString("auction_id")
    if chk == "" {
        api.Ctx.WriteString("")
        return
    }
    o := orm.NewOrm()
	o.Using("default")
	var Auctions [] ambilAuctions
	var sql string
	sql = "select auctions_id from auctions where auctions_id = '"+api.GetString("auction_id")+"'";
	num, err := o.Raw(sql).QueryRows(&Auctions)
	if err != orm.ErrNoRows && num > 0 {
		api.Data["json"] = ""
	    api.ServeJSON()   
	}
	sql = "UPDATE auctions SET status = 2 where auction_id = '"+api.GetStrings("auction_id")[0]+"'"	
	o.Raw(sql).QueryRows(&Auctions)
	api.Data["json"] = "successfully delete auctions with auction_id = "+api.GetStrings("auction_id")[0]
	api.ServeJSON()
}

func (api *AuctionsController) ExtendDueDate() {
    chk := api.GetString("due_date")
    if chk == "" {
        api.Ctx.WriteString("")
        return
    }
	auction_id := api.GetString("auction_id")
    if auction_id == "" {
        api.Ctx.WriteString("")
        return
    }
    o := orm.NewOrm()
	o.Using("default")
	var Auctions []*models.Auctions
	var sql string
	sql = "select auctions_id from auctions where auctions_id = '"+api.GetString("auction_id")+"'";
	num, err := o.Raw(sql).QueryRows(&Auctions)
	if err != orm.ErrNoRows && num > 0 {
		api.Data["json"] = ""
	    api.ServeJSON()   
	}
	sql = "UPDATE auctions SET auction_count = auction_count+1,due_date = '"+api.GetStrings("due_date")[0]+"' where auction_id = '"+api.GetStrings("auction_id")[0]+"'"
	o.Raw(sql).QueryRows(&Auctions)
	api.Data["json"] = "successfully extend date with auction_id = "+api.GetStrings("auction_id")[0]
	api.ServeJSON()
}

func (api *AuctionsController) ChooseBid() {
    accepted_bidder := api.GetString("accepted_bidder")
    if accepted_bidder == "" {
        api.Ctx.WriteString("")
        return
    }
	auction_id := api.GetString("auction_id")
    if auction_id == "" {
        api.Ctx.WriteString("")
        return
    }
    o := orm.NewOrm()
	o.Using("default")
	var Auctions []*models.Auctions
	var sql string
	sql = "select auctions_id from auctions where auctions_id = '"+api.GetString("auction_id")+"'";
	num, err := o.Raw(sql).QueryRows(&Auctions)
	if err != orm.ErrNoRows && num > 0 {
		api.Data["json"] = ""
	    api.ServeJSON()   
	}
	sql = "UPDATE auctions SET accepted_bidder = '"+api.GetStrings("accepted_bidder")[0]+"' where auction_id = '"+api.GetStrings("auction_id")[0]+"'"
	o.Raw(sql).QueryRows(&Auctions)
	api.Data["json"] = "successfully accept bidder to accepted_bidder = "+api.GetStrings("accepted_bidder")[0]+" on auction_id = "+api.GetStrings("auction_id")[0]
	api.ServeJSON()
}

func (api *AuctionsController) ChangeAuctionToProcess() {
    chk := api.GetString("auction_id")
    if chk == "" {
        api.Ctx.WriteString("")
        return
    }
    o := orm.NewOrm()
	o.Using("default")
	var Auctions []*models.Auctions
	var sql string
	sql = "select auctions_id from auctions where auctions_id = '"+api.GetString("auction_id")+"'";
	num, err := o.Raw(sql).QueryRows(&Auctions)
	if err != orm.ErrNoRows && num > 0 {
		api.Data["json"] = ""
	    api.ServeJSON()   
	}
	sql = "UPDATE auctions SET Status  = 2 where auction_id = '"+api.GetStrings("auction_id")[0]+"'"
	o.Raw(sql).QueryRows(&Auctions)
	api.Data["json"] = "successfully change auction to process with auction_id = "+api.GetStrings("auction_id")[0]
	api.ServeJSON()
}