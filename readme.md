Yang dikerjakan : 

membuat database :

1.	Collateral
CollID
AccID
TypeID : (1)
-	Gedung : 161
-	Gudang : 162
-	Ruko/Rukan/Kios : 163
-	Properti Komersil Lainnya : 175
-	Rumah Tinggal : 176
-	Apartemen / Rusun : 177
-	Tanah : 187
-	Kendaraan Bermotor : 189
OwnerName
CollLocation
InitialCollPrice
FinalCollPrice
LJKID
DocumentPath (Photos)

2.	Auctions
AuctionID
CollID
FromDate
DueDate
AuctionMethod : (2)
-	Self Selling : 1
-	Balai Lelang : 2
DownPayment
Description
AuctionCount
Status : (3)
-	Opened : 1 
-	On Process : 2
-	Sold Out : 3
-	Expired : 4
-	AcceptedBidder (updated)

3.	Biddings
BidID
AuctionID
BidderID
Status : (4)
-	Interested : 1
-	Joining : 2
-	Accepted : 3
-	Declined : 4

4.	UserAccount
UserID
Username
Password
Status : (5)
-	Inactived : 1
-	Actived : 2
-	Deactived : 3
KTPNumber

5.	PartnerAccount
PartnerID
Username
Password
LJKID
Status : (5)
-	Inactived : 1
-	Actived : 2
-	Deactived : 3
KTPNumber

6.	Logging
TrxID
TrxCategory
Details
Timestamp

Membuat Methods by Entities Pada route.go dan masing - masing controller :

1.	Collateral
-	GetAllCollateral
-	GetCollateralByID
-	GetCollateralByCategory
-	GetCollateralByLJK
-	GetCollateralByLocation
-	CreateCollateral
-	EditCollateral

2.	Auction
-	GetAllAuctions
-	GetAuctionsByCollateral
-	GetAuctionsByDueDate
-	CreateAuction
-	DeleteAuction
-	EditAuction
-	ChooseBid
-	ChangeAuctionToProcess
-	ExtendDueDate

3.	Bidding
-	CreateBid
-	PayBidDownPayment
-	GetBidsByAuction
-	GetAllBids
-	CancelBid

4.	UserAccount
-	CreateUser
-	EditUser
-	DeleteUser
-	AuthenticateUser
-	DeactivateUser
-	UserLogin

5.	PartnerAccount
-	CreateUser
-	EditUser
-	DeleteUser
-	AuthenticateUser
-	DeactivateUser
-	PartnerLogin

Membuat API testing dengan postman untuk masing masing Methods by Entities (terletak pada folder postman_collection)


Format SQL database :

--
-- PostgreSQL database dump
--

-- Dumped from database version 10.1
-- Dumped by pg_dump version 10.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: auctions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE auctions (
    auction_id integer NOT NULL,
    coll_id integer DEFAULT 0 NOT NULL,
    from_date date NOT NULL,
    due_date date NOT NULL,
    auction_method integer DEFAULT 0 NOT NULL,
    down_payment integer DEFAULT 0 NOT NULL,
    description text DEFAULT ''::text NOT NULL,
    auction_count integer DEFAULT 0 NOT NULL,
    status integer DEFAULT 0 NOT NULL,
    accepted_bidder integer DEFAULT 0 NOT NULL
);


ALTER TABLE auctions OWNER TO postgres;

--
-- Name: auctions_auction_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auctions_auction_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auctions_auction_id_seq OWNER TO postgres;

--
-- Name: auctions_auction_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE auctions_auction_id_seq OWNED BY auctions.auction_id;


--
-- Name: biddings; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE biddings (
    bid_id integer NOT NULL,
    auction_id integer DEFAULT 0 NOT NULL,
    bidder_id integer DEFAULT 0 NOT NULL,
    status integer DEFAULT 0 NOT NULL
);


ALTER TABLE biddings OWNER TO postgres;

--
-- Name: biddings_bid_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE biddings_bid_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE biddings_bid_id_seq OWNER TO postgres;

--
-- Name: biddings_bid_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE biddings_bid_id_seq OWNED BY biddings.bid_id;


--
-- Name: collateral; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE collateral (
    coll_id integer NOT NULL,
    acc_id integer DEFAULT 0 NOT NULL,
    type_id integer DEFAULT 0 NOT NULL,
    owner_name text DEFAULT ''::text NOT NULL,
    coll_location text DEFAULT ''::text NOT NULL,
    initial_col_price integer DEFAULT 0 NOT NULL,
    final_col_price integer DEFAULT 0 NOT NULL,
    ljk_id integer DEFAULT 0 NOT NULL,
    document_path text DEFAULT ''::text NOT NULL
);


ALTER TABLE collateral OWNER TO postgres;

--
-- Name: collateral_coll_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE collateral_coll_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE collateral_coll_id_seq OWNER TO postgres;

--
-- Name: collateral_coll_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE collateral_coll_id_seq OWNED BY collateral.coll_id;


--
-- Name: logging; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE logging (
    trx_id integer NOT NULL,
    trx_category integer DEFAULT 0 NOT NULL,
    details integer DEFAULT 0 NOT NULL,
    "timestamp" timestamp with time zone NOT NULL
);


ALTER TABLE logging OWNER TO postgres;

--
-- Name: m_option; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE m_option (
    id integer NOT NULL,
    type integer DEFAULT 0 NOT NULL,
    no integer DEFAULT 0 NOT NULL,
    name text DEFAULT ''::text NOT NULL
);


ALTER TABLE m_option OWNER TO postgres;

--
-- Name: m_option_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE m_option_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE m_option_id_seq OWNER TO postgres;

--
-- Name: m_option_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE m_option_id_seq OWNED BY m_option.id;


--
-- Name: partneraccount; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE partneraccount (
    partner_id integer NOT NULL,
    username text DEFAULT ''::text NOT NULL,
    password text DEFAULT ''::text NOT NULL,
    email text DEFAULT ''::text NOT NULL,
    ljk_id integer DEFAULT 0 NOT NULL,
    status integer DEFAULT 0 NOT NULL,
    ktp_no text DEFAULT ''::text NOT NULL
);


ALTER TABLE partneraccount OWNER TO postgres;

--
-- Name: partneraccount_partner_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE partneraccount_partner_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE partneraccount_partner_id_seq OWNER TO postgres;

--
-- Name: partneraccount_partner_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE partneraccount_partner_id_seq OWNED BY partneraccount.partner_id;


--
-- Name: useraccount; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE useraccount (
    user_id integer NOT NULL,
    username text DEFAULT ''::text NOT NULL,
    password text DEFAULT ''::text NOT NULL,
    email text DEFAULT ''::text NOT NULL,
    status integer DEFAULT 0 NOT NULL,
    ktp_no text DEFAULT ''::text NOT NULL
);


ALTER TABLE useraccount OWNER TO postgres;

--
-- Name: useraccount_user_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE useraccount_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE useraccount_user_id_seq OWNER TO postgres;

--
-- Name: useraccount_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE useraccount_user_id_seq OWNED BY useraccount.user_id;


--
-- Name: auctions auction_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auctions ALTER COLUMN auction_id SET DEFAULT nextval('auctions_auction_id_seq'::regclass);


--
-- Name: biddings bid_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY biddings ALTER COLUMN bid_id SET DEFAULT nextval('biddings_bid_id_seq'::regclass);


--
-- Name: collateral coll_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY collateral ALTER COLUMN coll_id SET DEFAULT nextval('collateral_coll_id_seq'::regclass);


--
-- Name: m_option id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY m_option ALTER COLUMN id SET DEFAULT nextval('m_option_id_seq'::regclass);


--
-- Name: partneraccount partner_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY partneraccount ALTER COLUMN partner_id SET DEFAULT nextval('partneraccount_partner_id_seq'::regclass);


--
-- Name: useraccount user_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY useraccount ALTER COLUMN user_id SET DEFAULT nextval('useraccount_user_id_seq'::regclass);


--
-- Data for Name: auctions; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO auctions VALUES (1, 1, '2019-08-02', '2019-12-02', 1, 1, '1', 1, 1, 1);
INSERT INTO auctions VALUES (3, 2, '2019-06-05', '2019-08-05', 2, 100000, 'note', 0, 1, 2);
INSERT INTO auctions VALUES (4, 2, '2019-06-05', '2019-08-05', 2, 100000, 'note', 0, 2, 0);
INSERT INTO auctions VALUES (2, 2, '2019-06-05', '2019-08-05', 2, 100000, 'note', 2, 1, 2);


--
-- Data for Name: biddings; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: collateral; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO collateral VALUES (2, 2, 162, 'ahmad', 'senayan', 1400000, 88888, 1, 'dx.docx');
INSERT INTO collateral VALUES (3, 4, 163, 'anji', 'jakarta', 18000000, 30000000, 2, 'result.docx');
INSERT INTO collateral VALUES (1, 4, 4, 'anji', 'jakarta', 18000000, 30000000, 2, 'result.docx');


--
-- Data for Name: logging; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: m_option; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO m_option VALUES (1, 1, 161, 'Gedung');
INSERT INTO m_option VALUES (2, 1, 162, 'Gudang');
INSERT INTO m_option VALUES (3, 1, 163, 'Ruko/Rukan/Kios ');
INSERT INTO m_option VALUES (4, 1, 175, 'Properti Komersil Lainnya');
INSERT INTO m_option VALUES (5, 1, 176, 'Rumah Tinggal');
INSERT INTO m_option VALUES (6, 1, 177, 'Apartemen / Rusun');
INSERT INTO m_option VALUES (7, 1, 187, 'Tanah ');
INSERT INTO m_option VALUES (8, 1, 189, 'Kendaraan Bermotor');
INSERT INTO m_option VALUES (9, 2, 1, 'Self Selling');
INSERT INTO m_option VALUES (10, 2, 2, 'Balai Lelang');
INSERT INTO m_option VALUES (11, 3, 1, 'Opened');
INSERT INTO m_option VALUES (12, 3, 2, 'On Process');
INSERT INTO m_option VALUES (13, 3, 3, 'Sold Out');
INSERT INTO m_option VALUES (14, 3, 4, 'Expired');
INSERT INTO m_option VALUES (15, 4, 1, 'Interested');
INSERT INTO m_option VALUES (16, 4, 2, 'Joining');
INSERT INTO m_option VALUES (17, 4, 3, 'Accepted');
INSERT INTO m_option VALUES (18, 4, 4, 'Declined');
INSERT INTO m_option VALUES (19, 5, 1, 'Inactived');
INSERT INTO m_option VALUES (20, 5, 2, 'Actived');
INSERT INTO m_option VALUES (21, 5, 3, 'Deactived');


--
-- Data for Name: partneraccount; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO partneraccount VALUES (1, 'dennis', 'dennis', 'dennis@yahoo.com', 1, 3, '13013201230');


--
-- Data for Name: useraccount; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO useraccount VALUES (2, 'maman', 'maman', 'maman@yahoo.com', 2, '31132312132');
INSERT INTO useraccount VALUES (1, 'dennis', 'dennis', 'manullang_d@yahoo.com', 3, '132132132132');


--
-- Name: auctions_auction_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('auctions_auction_id_seq', 4, true);


--
-- Name: biddings_bid_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('biddings_bid_id_seq', 1, false);


--
-- Name: collateral_coll_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('collateral_coll_id_seq', 3, true);


--
-- Name: m_option_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('m_option_id_seq', 1, false);


--
-- Name: partneraccount_partner_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('partneraccount_partner_id_seq', 1, true);


--
-- Name: useraccount_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('useraccount_user_id_seq', 2, true);


--
-- Name: auctions auctions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auctions
    ADD CONSTRAINT auctions_pkey PRIMARY KEY (auction_id);


--
-- Name: biddings biddings_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY biddings
    ADD CONSTRAINT biddings_pkey PRIMARY KEY (bid_id);


--
-- Name: collateral collateral_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY collateral
    ADD CONSTRAINT collateral_pkey PRIMARY KEY (coll_id);


--
-- Name: logging logging_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY logging
    ADD CONSTRAINT logging_pkey PRIMARY KEY (trx_id);


--
-- Name: m_option m_option_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY m_option
    ADD CONSTRAINT m_option_pkey PRIMARY KEY (id);


--
-- Name: partneraccount partneraccount_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY partneraccount
    ADD CONSTRAINT partneraccount_pkey PRIMARY KEY (partner_id);


--
-- Name: partneraccount partneraccount_username_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY partneraccount
    ADD CONSTRAINT partneraccount_username_key UNIQUE (username);


--
-- Name: useraccount useraccount_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY useraccount
    ADD CONSTRAINT useraccount_pkey PRIMARY KEY (user_id);


--
-- Name: useraccount useraccount_username_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY useraccount
    ADD CONSTRAINT useraccount_username_key UNIQUE (username);


--
-- PostgreSQL database dump complete
--


